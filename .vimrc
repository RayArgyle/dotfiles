" 13.04.19

set background=light
set shiftwidth=4
set termencoding=utf-8 
set laststatus=2 
set t_Co=256
set rulerformat=%l\:%c
set scrolloff=4
set colorcolumn=80
set autowrite
set nocompatible
set incsearch
set autoindent
set nu 
set relativenumber
set cursorline
set mousehide 
set nobackup
set noswapfile
set ai
set si
set splitbelow splitright
set mousehide 
set noshowmode 


filetype on
filetype plugin on


let python_highlight_all = 1
let g:NERDTreeWinPos = "right"

"colorscheme Iosvkem 
"colorscheme mysticaltutor
colorscheme solarized


syntax enable
syntax on 


map 0 ^
map  <space> :
nnoremap <C-F8> :nohlsearch<CR>


so ~/.vim/plugins.vim


"autocmd vimenter * NERDTree
"autocmd VimEnter * wincmd p
autocmd BufNewFile, BufRead *.md set syntax=markdown tabstop=2 shiftwidth=2 filetype=markdown
autocmd FileType markdown :Goyo | set filetype=markdown syntax=markdown


Plugin 'junegunn/goyo.vim'
Plugin 'tpope/vim-surround'
