#path in the title for GNOME terminal

case "$TERM" in
 xterm*|rxvt*)
     # PS1="[\\$] \[$(tput sgr0)\]\[\e]0;\w\a\]"
     # PS1="-> \[$(tput sgr0)\]\[\e]0;\w\a\]"
     # PS1="[⏵] \[$(tput sgr0)\]\[\e]0;\w\a\]"
     # PS1="\[\033[m\]|\[\033[1;35m\]\t\[\033[m\]| \[\033[38;5;11m\] \W \[\e[0m\]\[\e[0m\]"
     PS1=" • \[\033[38;5;11m\] \W \[\e[0m\]\[\e[0m\] "
     ;;
 *)
     ;;
esac



force_color_prompt=yes

#for colors to work with beach theme
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

#useful aliases
alias python='python3'
alias pip='pip3'
alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -i'
alias du='du -h'
alias ..='cd ..'
alias pydb="source ~/DjangoEnvs/main_env/bin/activate ;python3 manage.py runserver"
alias lsa='ls -a'
alias chrome='google-chrome 2>/dev/null 1>&2 &'
alias firefox='firefox 2>/dev/null 1>&2 &'
alias jobs='jobs -l'
alias trello='~/Trello/Trello 2>/dev/null 1>&2 &'
alias telegram='~/Telegram/Telegram 2>/dev/null 1>&2 &'
alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME' 
alias neofetch='clear;neofetch'

export EDITOR=vim
set -o vi
